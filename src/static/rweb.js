$( document ).ready(function() {
	jQuery.fn.reverse = [].reverse;
	var rolls = new Array();
	var updating = false;
	$('#container').isotope({
	  layoutMode: 'fitColumns',
	  resizesContainer: false,
	});
	function update(){
		if( updating ){ return; }
		updating = true;
		$.ajax({url:'rolls', data:{rolls:JSON.stringify(rolls)},
		       success:function(data){
							if( data.length != 0 ){
								var $pops = $('#container').data('isotope')
								          .$filteredAtoms.filter( function( i ) {
								            return i > 10;
								          });
								$('.number').removeAttr('id');
								var newItems = $(data);
								newItems.first().children('div').attr('id','lastroll');
								$('#container').prepend(newItems)
				  				.isotope( 'reloadItems' )
				  				.isotope( 'remove', $pops )
				  				.isotope({ sortBy: 'original-order' });
				  				
				  			newItems.reverse().each(function(index, value){
				  				rolls.splice(0, 0, this.innerText);
				  			});
				  			rolls.splice(20, 100);
							}
					  },
					dataType:'text', async:false});
		updating = false;
	}
	update();
	setInterval(update, 1000);
});
