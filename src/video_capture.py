from glob import glob
import cv2

class CaptureMonitor:
    def __init__(self, capture, cfg):
        self.main_config = cfg
        self.capture_config = {}
        self.capture = capture
        self.configure_capture()

    def release(self):
        self.capture.release()
        
    def get_im(self):
        return self.capture.read()[1]
        
    def configure_capture(self):
        cur_cfg = self.get_cv_capture_config()
        for cv_code, cur_val in cur_cfg.iteritems():
            old_val = self.capture_config.get(cv_code)
            if old_val != cur_val:
                print 'SET %s: %s'%(cv_code, self.capture.set(cv_code, cur_val))
        self.capture_config = cur_cfg
    
    def get_cv_capture_config(self):
        vals = {}
        vals[ getattr(cv2.cv, 'CV_CAP_PROP_EXPOSURE') ] = -6   ## Hard code exposure setting
        for opt in self.main_config.config.values():
            if opt.name.startswith('CAPTURE'):
                cv_name = opt.name.replace('CAPTURE', 'CV_CAP_PROP')
                cv_code = getattr(cv2.cv, cv_name)
                vals[cv_code] = opt.val
        return vals

class MockCapture:
    def __init__(self, ims):
        self.ims = ims
        self.idx = 0
        
    def get_im(self):
        im = self.ims[self.idx]
        self.idx = (self.idx + 1) % len(self.ims)
        return cv2.imread(im)
    
    def configure_capture(self):
        pass

    def release(self):
        pass

def get_camera_capture(cam_idx, cfg):
    capture = cv2.VideoCapture(cam_idx)
    return CaptureMonitor(capture, cfg)

def get_test_capture():
    ims = sorted(glob(r'C:\Users\azimme\Desktop\~\Roulette\test_capture\*.jpg'))
    return MockCapture(ims)
