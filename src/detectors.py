import cv2
import numpy as np

class Detector:
    def __init__(self, cfg):
        self.config = cfg
        self.center_d = CenterDetector(cfg)
        self.wheel_d = WheelDetector(cfg)
        self.ball_d = BallDetector(cfg)

    def detect_center(self, im):
        return self.center_d.detect(im)
    
    def detect_wheel(self, center, im):
        return self.wheel_d.detect(center, im)
    
    def detect_ball(self, wheel, im):
        return self.ball_d.detect(wheel, im)

class BallDetector:
    def __init__(self, cfg):
        self.config = cfg
        self.config_fs = 'WHITE_H_MIN WHITE_H_MAX WHITE_S_MIN WHITE_S_MAX WHITE_V_MIN WHITE_V_MAX'.split()

    def get_ranges(self):
        return tuple(getattr(self.config, f) for f in self.config_fs)

    def detect(self, wheel, im):
        '''Return the number that the ball has landed in, or None if not detected'''

        #_im = isolate_hsv(im, self.get_ranges())
        lum = cv2.cvtColor(im, cv2.COLOR_RGB2YCR_CB)[:,:,0]       # isolate the luminance channel
        _im = cv2.threshold(lum, self.config.BALL_LUMINOSITY, 255, cv2.THRESH_BINARY_INV)[1] # drop out low values

        i_radius = int(wheel['radius'] * self.config.BALL_RADIUS_MIN_PC/100.)
        o_radius = int(wheel['radius'] * self.config.BALL_RADIUS_MAX_PC/100.)

        cv2.circle(_im, wheel['center'], i_radius, (0,0,255), 3)
        cv2.circle(_im, wheel['center'], o_radius, (0,0,255), 3)

        ## Filter out contours, looking for the ball
        contours, hierarchy = cv2.findContours( _im.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        contours = [cv2.approxPolyDP(cnt, 3, True) for cnt in contours]
        contours = [cnt for cnt in contours if self.contour_in_wheel(cnt, wheel['center'], i_radius, o_radius)]  ## Filter by position
        contours.sort(key=lambda cnt: cv2.arcLength(cnt, closed=True) / cv2.contourArea(cnt))
        contours = contours[:1]

        cv2.drawContours(_im, contours, -1, 128, 1, cv2.CV_AA)

        if len(contours) != 1:
            return {}, _im

        x,y = centroid(contours[0])
        angle = get_angle(wheel['center'], (x,y))
        num = sorted(wheel['wheel'], key=lambda (_,ang):abs(ang-angle))[0][0]

        ## Debug output ##
        fargs = dict(text="Ball in slot %s"%num, fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, thickness=2)
        sz = cv2.getTextSize(**fargs)[0]
        fargs.update(img=_im, color=(200,255,255), org=(20,20))
        cv2.putText(**fargs)

        return dict(number=num, angle=angle), _im

    def contour_in_wheel(self, contour, (x0,y0), i_r, o_r):
        x,y = centroid(contour)
        r = np.sqrt((x-x0)**2 + (y-y0)**2)
        return i_r < r < o_r


class WheelDetector:
    def __init__(self, cfg):
        self.config = cfg
        #self.config_fs = 'RED_H_MIN RED_H_MAX RED_S_MIN RED_S_MAX RED_V_MIN RED_V_MAX'.split()
        self.config_fs = 'REDRGB_B_MIN REDRGB_B_MAX REDRGB_G_MIN REDRGB_G_MAX REDRGB_R_MIN REDRGB_R_MAX'.split()

        self.start_angle0 = cfg.WHEEL_ANGLE_DELTA*4 - 5
        self.start_angle1 = cfg.WHEEL_ANGLE_DELTA*4 + 5
        self.wheel_nums = list(reversed('2 0 28 9 26 30 11 7 20 32 17 5 22 34 15 3 24 36 13 1 00 27 10 25 29 12 8 19 31 18 6 21 33 16 4 23 35 14'.split()))
       
    def get_ranges(self):
        return tuple(getattr(self.config, f) for f in self.config_fs)
    
    def detect(self, center, im):
        ## First look for contours of wheel slots
        #_im = isolate_hsv(im, self.get_ranges())
        _im = isolate_rgb(im, self.get_ranges())
        cv2.circle(_im, center, self.config.WHEEL_SLOT_R0, (0,0,255), 2)
        cv2.circle(_im, center, self.config.WHEEL_SLOT_R1, (0,0,255), 2)
        
        contours, hierarchy = cv2.findContours( _im.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        def filter_contour(c):
            return self.config.WHEEL_SLOT_SIZE_MIN <= cv2.contourArea(c) <= self.config.WHEEL_SLOT_SIZE_MAX and \
                self.config.WHEEL_SLOT_R0 <= get_radius(center, centroid(c)) <= self.config.WHEEL_SLOT_R1
        contours = filter(filter_contour, contours)
        centers = map(centroid, contours)
        angles = [get_angle(center,p1) for p1 in centers]
        contours = sorted([dict(contour=contour,center=_center,angle=angle) for contour,_center,angle in zip(contours,centers,angles)], key=lambda c:c['angle'])
        pops = [x+1 for x,(c1,c2) in enumerate(zip(contours,contours[1:])) if c2['angle']-c1['angle'] < self.config.WHEEL_ANGLE_DELTA/2]
        [contours.pop(x) for x in reversed(pops)]

        cv2.drawContours(_im, [c['contour'] for c in contours], -1, (128,255,255), 3, cv2.CV_AA)

        ## Perform further processing on the contours
        pts = [c['center'] for c in contours]
        try:
            (x00,y00), radius = cv2.minEnclosingCircle(np.array(pts, np.int32))         # get the circle that encloses the centroids
        except:
            return {}, _im

        ## Debug output ##
        cv2.circle(_im, (int(x00),int(y00)), 2, (0,0,255), 2)
        cv2.circle(_im, (int(x00),int(y00)), int(radius*1.1), (188,255,255),1, cv2.CV_AA)

        ## Perform some computations on the points
        pts = sorted((get_angle(center, pt), pt) for pt in pts)     ## Store the additional info along with each point
        deltas = [p2[0]-p1[0] for p1,p2 in zip(pts, pts[1:])]     ## Calculate deltas between angles
        start_idx = ([x+1 for x,a in enumerate(deltas) if self.start_angle0 <= a <= self.start_angle1]+[0])[0]  ## Look for the large gap, this is our starting point 
        last_idx = start_idx-1 if start_idx > 0 else len(pts)-1  ## Also remember the index of the last point

        ## Calculate positions of missing (red and green) numbers
        all_angles = []
        for idx, (ang,pt) in enumerate(pts):
            if idx == start_idx: all_start_idx = len(all_angles)                ## Remember position of starting number 13
            nxt_ang = pts[0][0]+360 if idx == len(pts)-1 else pts[idx+1][0]     ## Get the next angle in the list
            num_to_fill = 3 if idx == last_idx else 1                           ## Special case for last number
            delta = (nxt_ang-ang) / float(num_to_fill+1)                        ## Calculate angle delta
            all_angles.append(ang)                                              ## Store this point first with starting point flag
            all_angles.extend([ang+delta*j for j in xrange(1, num_to_fill+1)])  ## Store the fill in points
        all_angles = all_angles[all_start_idx:] + all_angles[:all_start_idx]
        wheel = zip(self.wheel_nums, all_angles)
        if len(wheel) != len(self.wheel_nums):
            return {}, _im

        ## Debug output ##
        for num, angle in wheel:
            x,y = get_point(center, angle, radius*1.5)
            fargs = dict(text=str(num), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, thickness=1)
            sz = cv2.getTextSize(**fargs)[0]
            fargs.update(img=_im, color=(200,255,255), org=(x-sz[0]/2, y+sz[1]/2))
            cv2.putText(**fargs)

        return dict(wheel=wheel, radius=radius, center=center), _im

class CenterDetector:
    def __init__(self, cfg):
        self.config = cfg
        self.config_fs = 'GOLD_H_MIN GOLD_H_MAX GOLD_S_MIN GOLD_S_MAX GOLD_V_MIN GOLD_V_MAX'.split()
        self.center = {}
        self.setup_mouse()

    def setup_mouse(self):
        def on_mouse(event, x, y, flags, param):
            if event == cv2.EVENT_LBUTTONDOWN:
                self.center = (x,y)
        cv2.setMouseCallback(self.config.MAIN_WINDOW_NAME, on_mouse)
        
    def get_ranges(self):
        return tuple(getattr(self.config, f) for f in self.config_fs)
    
    def detect(self, im):
        #_im = isolate_hsv(im, self.get_ranges())
        #circles = cv2.HoughCircles(_im, cv2.cv.CV_HOUGH_GRADIENT, 1, 3)
        if self.center:
            cv2.circle(im, self.center, 4, (0,0,255), 2)
        return self.center, im
        
def isolate_hsv(im, ranges, invert=True):
    hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
    h0,h1,s0,s1,v0,v1 = ranges
    lower = np.array([h0,s0,v0])
    upper = np.array([h1,s1,v1])
    mask = cv2.inRange(hsv, lower, upper)
    flag = cv2.THRESH_BINARY if not invert else cv2.THRESH_BINARY_INV
    return cv2.threshold(mask, 254, 255, flag)[1]

def isolate_rgb(im, ranges, invert=True):
    h0,h1,s0,s1,v0,v1 = ranges
    lower = np.array([h0,s0,v0])
    upper = np.array([h1,s1,v1])
    mask = cv2.inRange(im, lower, upper)
    flag = cv2.THRESH_BINARY if not invert else cv2.THRESH_BINARY_INV
    return cv2.threshold(mask, 254, 255, flag)[1]

def centroid(contour):
    try:
        m = cv2.moments(contour)
        return (int(m['m10']/m['m00']), int(m['m01']/m['m00']))  # calculate centroid of contour
    except:
        return -1,-1
    
def get_radius(p0, p1):
    x0,y0 = p0
    x1,y1 = p1
    return np.sqrt((x1-x0)**2 + (y1-y0)**2)

def get_angle((x0,y0),(x1,y1)):
    '''Return angle of point (x,y) with respect to center point (x0,y0).
       Angle is measured counter-clockwise and 0 <= angle < 360.'''
    return np.arctan2(y0-y1, x1-x0)*180/np.pi + (get_quadrant((x0,y0),(x1,y1))-1)/2*360

def get_quadrant((x0,y0),(x,y)):
    return (x-x0)>=0 and (y0-y)>=0 and 1 or (x-x0)<0 and (y0-y)>=0 and 2 or (x-x0)<0  and (y0-y)<0  and 3 or (x-x0)>=0 and (y0-y)<0 and 4

def get_point(center, angle, radius):
    x0,y0 = center
    return (int(radius*np.cos(angle*np.pi/180.0)+x0), int(y0-radius*np.sin(angle*np.pi/180.0))) # Angle to point in image
