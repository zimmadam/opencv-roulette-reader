import json
import cv2
import numpy as np
import os, time

class ConfigOption:
    def __init__(self, name, val, max_val=None, min_val=0):
        self.name = name
        self.val = val
        self.min_val = min_val
        self.max_val = max_val

    def json(self):
        return dict(name=self.name, val=self.val, min_val=self.min_val, max_val=self.max_val)
        
    def set_scaled_val(self, cur_val):
        self.val = int(self.min_val + cur_val * (self.max_val - self.min_val) / 100)
        
    def get_scaled_val(self):
        return int(100 * (self.val - self.min_val) / (self.max_val - self.min_val))

    def use_trackbar(self):
        return self.max_val != None

class RouletteConfig:
    def __init__(self, path='globals.json', win_name='config', defaults=()):
        self.path = path
        self.win_name = win_name
        self.config = self.load_config()
        [self.add(opt) for opt in defaults]
        self.init_trackbars()
        
    def __getattr__(self, name):
        if name in self.config:
            return self.config[name].val
        raise AttributeError
    
    def init_trackbars(self):
        win_names = set()
        for var_name, opt in self.config.items():
            win_name = '%s %s'%(var_name.split('_')[0], self.win_name)
            if opt.use_trackbar() and win_name not in win_names:
                win_names.add(win_name)
                cv2.namedWindow(win_name, cv2.WINDOW_NORMAL|cv2.WINDOW_AUTOSIZE)
                cv2.imshow(win_name, np.zeros((50, 300, 1), np.uint8))
            if opt.use_trackbar():
                cv2.createTrackbar('_'.join(var_name.split('_')[1:]), win_name, opt.get_scaled_val(), 100, self.get_updater(var_name))
                
    def get_updater(self, var_name):
        def update_cb(cur_val):
            self.config[var_name].set_scaled_val(cur_val)
            self.save_config()
        return update_cb

    def add(self, opt):
        self.config.setdefault(opt.name, opt)

    def json(self):
        return dict( (opt.name, opt.json()) for opt in self.config.values() )

    def load_config(self):
        if os.path.exists(self.path):
            with open(self.path) as fh:
                config = json.load(fh)
                return dict( (name, ConfigOption(**values)) for name, values in config.iteritems() )
        else:
            return {}
         
    def save_config(self):
        with open('globals.json', 'w') as fh:
            json.dump(self.json(), fh, indent=2, sort_keys=True)

DEFAULTS = [
            ConfigOption('MAIN_WINDOW_NAME', 'Camera Feed'),
            
            ConfigOption('REDRGB_R_MIN', 144, 255),
            ConfigOption('REDRGB_R_MAX', 179, 255),
            ConfigOption('REDRGB_G_MIN', 111, 255),
            ConfigOption('REDRGB_G_MAX', 224, 255),
            ConfigOption('REDRGB_B_MIN', 35, 255),
            ConfigOption('REDRGB_B_MAX', 58, 255),
            
            ConfigOption('CAPTURE_FRAME_HEIGHT',      720),
            ConfigOption('CAPTURE_FRAME_WIDTH',       960),
            ConfigOption('CAPTURE_BRIGHTNESS',  32, 100),
            ConfigOption('CAPTURE_SATURATION',  32, 100),
            ConfigOption('CAPTURE_HUE',         13, 100),
            ConfigOption('CAPTURE_GAIN',        66, 100),
            
            ConfigOption('WHEEL_SLOT_SIZE_MIN', 20, 5000),
            ConfigOption('WHEEL_SLOT_SIZE_MAX', 1000, 5000),
            ConfigOption('WHEEL_SLOT_R0', 70, 300),
            ConfigOption('WHEEL_SLOT_R1', 80, 300),
            ConfigOption('WHEEL_ANGLE_DELTA', 360/38.0),

            ConfigOption('BALL_RADIUS_MIN_PC', 70, 300),
            ConfigOption('BALL_RADIUS_MAX_PC', 120, 300),
            ConfigOption('BALL_HISTORY', 50, 100),
            ConfigOption('BALL_DETECT', 70, 100),
            ConfigOption('BALL_UNDETECT', 50, 100),
            ConfigOption('BALL_LUMINOSITY', 200, 255),

##            ConfigOption('RED_H_MIN', 144, 179),
##            ConfigOption('RED_H_MAX', 179, 179),
##            ConfigOption('RED_S_MIN', 111, 255),
##            ConfigOption('RED_S_MAX', 224, 255),
##            ConfigOption('RED_V_MIN', 35, 255),
##            ConfigOption('RED_V_MAX', 58, 255),

##            ConfigOption('WHITE_H_MIN', 127, 179),
##            ConfigOption('WHITE_H_MAX', 179, 179),
##            ConfigOption('WHITE_S_MIN', 13, 255),
##            ConfigOption('WHITE_S_MAX', 67, 255),
##            ConfigOption('WHITE_V_MIN', 19, 255),
##            ConfigOption('WHITE_V_MAX', 42, 255),

##            ConfigOption('GOLD_H_MIN', 0, 179),
##            ConfigOption('GOLD_H_MAX', 102, 179),
##            ConfigOption('GOLD_S_MIN', 21, 255),
##            ConfigOption('GOLD_S_MAX', 146, 255),
##            ConfigOption('GOLD_V_MIN', 36, 255),
##            ConfigOption('GOLD_V_MAX', 91, 255),
            
##            ConfigOption('CAPTURE_EXPOSURE',    -6, 10, -10),
            ]

def get_default_config():
    return RouletteConfig(defaults=DEFAULTS)

if __name__ == '__main__':
    try:
        config = get_default_config()
        while True:
            if cv2.waitKey(5) == 27: break
            time.sleep(0.1)
    finally:
        cv2.destroyAllWindows()
