import cv2
import config
import detectors
import video_capture
import traceback
import time
from collections import deque
from datetime import datetime as dt

class RouletteReader(object):
    def __init__(self, capture, cfg):
        self.config = cfg
        self.capture = capture
        self.ims = [None]*4
        self.im_idx = 0
        self.detector = detectors.Detector(cfg)
        
    def show(self):
        if self.ims[self.im_idx] is not None:
            cv2.imshow(self.config.MAIN_WINDOW_NAME, self.ims[self.im_idx])

    def analyze_history(self, balls, current_ball):
        counts = {}
        for ball in balls[-self.config.BALL_HISTORY:]:
            counts[ball] = counts.get(ball,0)+1
        ordered_balls = sorted(counts.iteritems(), key=lambda (k,v): v)

        ## Debug output ##
        if self.ims[-1] is not None:
            msg = ' | '.join('%s:%d'%ob for ob in reversed(ordered_balls))
            fargs = dict(text=msg, fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.6, thickness=1)
            sz = cv2.getTextSize(**fargs)[0]
            fargs.update(img=self.ims[-1], color=(200,255,255), org=(20,50))
            cv2.putText(**fargs)
            
        if current_ball:
            confidence = counts.get(current_ball, 0) / float(self.config.BALL_HISTORY)
            if confidence < self.config.BALL_UNDETECT / 100.:
                return -1

        confidence = ordered_balls[-1][1] / float(self.config.BALL_HISTORY)
        if confidence > self.config.BALL_DETECT / 100.:
            return ordered_balls[-1][0]

    def register_new_ball(self, ball):
        with open('ball_history.txt', 'a') as f:
          f.write('%s\n'%ball)

    def run(self):
        current_ball = None
        balls = deque(maxlen=100)
        for ball in self.read_forever():
            if ball is None: continue
            balls.append(ball.get('number'))
            
            new_ball = self.analyze_history(list(balls), current_ball)
            if new_ball == -1 and current_ball:
                current_ball = None
                print 'Reseting ball'
            elif new_ball not in (None,-1) and new_ball != current_ball:
                self.register_new_ball(new_ball)
                current_ball = new_ball
                print 'Detected ball', new_ball
        
    def read_forever(self):
        while True:
            self.capture.configure_capture()
            im = self.capture.get_im()
            self.ims[0] = im
            self.show()
            
            ch = 0xFF & cv2.waitKey(10)
            if ch == 27: 
                break
            elif ch == ord('n'):
                self.im_idx = (self.im_idx + 1) % len(self.ims)
            elif ch == ord('p'):
                self.im_idx = (self.im_idx - 1) % len(self.ims)

            try:
                center, _im1 = self.detector.detect_center(im)
                self.ims[1] = _im1
                if not center:
                    yield None
                    continue
                
                wheel, _im2 = self.detector.detect_wheel(center, im)
                self.ims[2] = _im2
                if not wheel:
                    yield None
                    continue
                
                ball, _im3 = self.detector.detect_ball(wheel, im)
                self.ims[3] = _im3
                if not ball:
                    yield {}
                    continue

                yield ball
            except:
                traceback.print_exc()
                time.sleep(1)
                continue

def main():
    cfg = config.get_default_config()
    cv2.namedWindow(cfg.MAIN_WINDOW_NAME)
    capture = video_capture.get_camera_capture(0, cfg)
    #capture = video_capture.get_test_capture()
    rr = RouletteReader(capture, cfg)
    rr.run()
    capture.release()

if __name__ == '__main__':
    try:
        main()
    finally:
        cv2.destroyAllWindows()
