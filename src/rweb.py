from datetime import datetime as dt
from functools import partial
from itertools import islice
import datetime
import json
import web
import os

urls = ('/rolls', 'rolls',
        '/','index')

#dumps = partial(json.dumps, default=lambda obj: obj.isoformat() if isinstance(obj, dt) else None)
render = web.template.render('templates/')
max_rolls = 12
d = dict(green='0 00', red='27 25 12 19 18 21 16 23 14 9 30 7 32 5 34 3 36 1')
colors = dict((num,color) for color,nums in d.iteritems() for num in nums.split())

class rolls:
   def GET(self):
      cur_rolls = '|'.join(roll.strip() for roll in json.loads(web.input(rolls='[]').rolls))
      rolls = []
      if os.path.exists('ball_history.txt'):
         with open('ball_history.txt') as f:
            rolls = '|'.join(reversed(f.read().split()))

      if cur_rolls:
         rolls = rolls[:rolls.index(cur_rolls)].strip('|')
      new_rolls = ''.join(self.get_roll_div(roll, x==0) for x,roll in enumerate(rolls.split('|')[:max_rolls])) if rolls else ''
      return new_rolls
   
   def get_roll_div(self, roll, lastroll):
      args = (colors.get(roll,'black'), roll.strip())
      return '<div class="roll"><div class="number %s">%s</div></div>' % args
      
class index:
   def GET(self):
      return render.index()

if __name__ == "__main__": 
   app = web.application(urls, globals())
   app.run()
